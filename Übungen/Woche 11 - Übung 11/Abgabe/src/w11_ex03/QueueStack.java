package w11_ex03;

import java.util.ArrayDeque;
import java.util.EmptyStackException;

public class QueueStack implements AbstractQueueStack<Integer> {

    private ArrayDeque<Integer> inputQueue;
    private ArrayDeque<Integer> outputQueue;
    private int size;

    public QueueStack() {
        inputQueue = new ArrayDeque<>();
        outputQueue = new ArrayDeque<>();
        size = 0;
    }

    @Override
    public Integer pop() {
        if (outputQueue.isEmpty()) {
            throw new EmptyStackException();
        }
        size--;
        return outputQueue.remove();
    }

    @Override
    public void push(Integer element) {
        inputQueue.add(element);

        if (!inputQueue.isEmpty()) {
            outputQueue.addFirst(inputQueue.remove());
            size++;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Integer size() {
        return size;
    }

    @Override
    public Integer top() {
        return outputQueue.getLast();
    }

    public void printStack() {
        outputQueue.forEach((System.out::println));
    }
}
