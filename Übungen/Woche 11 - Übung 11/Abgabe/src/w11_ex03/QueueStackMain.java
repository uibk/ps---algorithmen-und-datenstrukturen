package w11_ex03;

public class QueueStackMain {
    public static void main(String[] args) {
        QueueStack queueStack = new QueueStack();
        System.out.println("Pushing ... \n");
        for (int i = 1; i < 6; i++) {
            queueStack.push(i);
        }
        System.out.println("Whole Stack\n");
        queueStack.printStack();

        System.out.println("Top element = " + queueStack.top());

        System.out.println("Whole Stack\n");
        queueStack.printStack();

        while (!queueStack.isEmpty()) {
            System.out.println("Popped element = " + queueStack.pop());
            queueStack.printStack();
        }

    }

}
