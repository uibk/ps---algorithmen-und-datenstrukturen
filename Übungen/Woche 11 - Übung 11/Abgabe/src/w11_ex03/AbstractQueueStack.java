package w11_ex03;

public interface AbstractQueueStack<T> {

    T pop();

    void push(T element);

    boolean isEmpty();

    T size();

    T top();
}
