package w11_ex02;

public class EditDistance {

    public static void main(String[] args) {
        EditDistance edit = new EditDistance();
        long start = System.nanoTime();
        System.out.println(edit.editDistance("Deterministic", "Algorithmic"));
        long end = System.nanoTime();
        System.out.println("Time taken=" + (end - start) + "ns");
    }

    /*
    Original Solution:
    If m is length of String x and n is length of String y -> The time complexity of the original solution was exponential. The worst case could end up in O(3^m) complexity.
    It happens, when none of the characters are matching. Many subproblems are solved over and over again. It has the property of overlapping subproblems.
    As this problem has the property of optimal substructure (optimal solution can be obtained using optimal subproblems) it is sufficient for dynamic programming (Optimal Substructure and Overlapping Subproblems)

    Modified solution:
    First of all, we need an temporary array to store already existing solutions (memoization) . This reduces our time complexity immense.
    And then, instead of using recursion we take the bottom-up approach and replace the recursive calls to two for loops which fills our matrix.
    We can then approach this problem of filling in our first row and column when we would compare one String (either m or n) to a null String.
    When String m has 5 Characters it would need 5 operations to get the same result as an empty String.
    With these solutions we can fill up our matrix and take the minimum of diagonal, horizontal and vertical solutions and just add 1 to the previous obtained result, if the character doesn't match.
    The conclusion is a time complexity of O(m * n) and space complexity of same O(m * n) which represents the lengths of both Strings iterable trough two for loops.
    */
    public int editDistance(String x, String y) {

        int x_len = x.length();
        int y_len = y.length();

        int[][] resultArray = new int[x_len + 1][y_len + 1];
        // Fill in first column with 0 to length of x
        // as each additional character takes 1 more operation
        for (int i = 0; i < x_len; i++) {
            resultArray[i][0] = i;
        }
        // Same for first row of y
        for (int i = 0; i < y_len; i++) {
            resultArray[0][i] = i;
        }
        for (int i = 1; i <= x_len; i++) {
            for (int j = 1; j <= y_len; j++) {

                // Extract last char of x and y
                char last_x = x.charAt(i - 1);
                char last_y = y.charAt(j - 1);

                // Case for deleting the last character from x
                int vertical = resultArray[i - 1][j];
                // Case for inserting a new character at the end of x
                int horizontal = resultArray[i][j - 1];
                // Case for the last characters of x and y matching,
                // or the last char of x needs to be replaced by another
                int diagonal = resultArray[i - 1][j - 1];

                // If last characters match, take result from diagonal
                if (last_x == last_y) {
                    resultArray[i][j] = diagonal;
                } else {
                    // Otherwise take minimum of top (vertical), left (horizontal) and diagonal and add 1 to it
                    resultArray[i][j] = 1 + Math.min(diagonal, Math.min(vertical, horizontal));
                }
            }
        }
        // Result is value of down right corner in matrix
        return resultArray[x_len][y_len];
    }
}
