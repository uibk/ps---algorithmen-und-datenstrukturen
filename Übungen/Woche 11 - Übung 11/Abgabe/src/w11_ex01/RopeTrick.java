package w11_ex01;

public class RopeTrick {
    public static void main(String[] args) {
        int[] ropes = new int[10];
        fillPrices(ropes);
        System.out.println(cutRope(4, ropes));
    }

    private static void fillPrices(int[] ropes) {
        ropes[0] = 1;
        ropes[1] = 5;
        ropes[2] = 8;
        ropes[3] = 9;
        ropes[4] = 10;
        ropes[5] = 17;
        ropes[6] = 17;
        ropes[7] = 20;
        ropes[8] = 24;
        ropes[9] = 30;
    }

   /* First solution without temporary array using recursion
   public static int cutRope(int n, int[] ropePrices) {
        if (n < 2) {
            return 0;
        }
        int result = 0;

        for (int i = 1; i < n; i++) {
            result = Math.max(result, ropePrices[i] + cutRope(n - i - 1, ropePrices));
        }
        return result;
    }*/

    // Second solution with temporary array for memoization and iterative bottom-up approach
    public static int cutRope(int n, int[] ropePrices) {
        // Create temporary array for storing already existing solutions
        int[] tempArray = new int[n + 1];

        for (int i = 1; i <= n; i++) {
            int result = 0;
            for (int j = 0; j < i; j++) {
                result = Math.max(result, ropePrices[j] + tempArray[i - j - 1]);
                tempArray[i] = result;
            }
        }
        return tempArray[n];
    }
}
