---
title: Algorithmen und Datenstrukturen Woche 13
author: Andreas Murk
---

Exercise 01
===

### 1. Huffman Coding Tree

![Huffman Coding Tree](./w14_ex01/huffmann_tree_1.png){width=50%}
![Huffman Coding Tree](./w14_ex01/huffmann_tree_2.png){width=50%}
![Huffman Coding Tree](./w14_ex01/huffmann_tree_3.png){width=50%}
![Huffman Coding Tree](./w14_ex01/huffmann_tree_4.png){width=50%}

$$
$$

$$
$$
$$
$$

#### Character coding

![Huffman Coding Tree](./w14_ex01/huffmann_tree_5.png){width=40%}

### 2. Decoding String - Pseudocode 

```Java
public String getEncodedString(Tree tree, String encodedString) {
    StringBuilder decodedString = new StringBuilder();
    
    while(encodedString.size() > 0) {
        Node current = tree.root();

        // traverse all nodes until children
        // are null (no children left)

        // only nodes without children (leafs)
        // store characters instead of occurences
        while(current.children() != null) {
            // Remove char at first position of String
            char c = encodedString.removeCharAt(0);

            // Assume that character stores its coding
            if (Integer.parseInt(c) == 0) {
                // Left traversal has value 0
                current = current.getLeft();
            } else {
                // Right traversal has value 1
                current = current.getRight();
            }
        }
        decodedString.append(c);
    }
    return decodedString.toString();
}
```

### 3. Decode the String

$$101|100|01|1100|101|01|1111$$

$$101 = e$$
$$100 = a$$
$$01 = s$$
$$1100 = i$$
$$101 = e$$
$$01 = s$$
$$1111 = t$$

$$
$$
$$
$$
$$
$$
$$
$$