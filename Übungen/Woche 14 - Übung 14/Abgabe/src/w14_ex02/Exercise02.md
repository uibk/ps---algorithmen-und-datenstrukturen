
Exercise 02
===

### 1. memoized with recursion

When a recursive function needs to calculate something like the Fibonacci-Numbers, we can see that many calls are calculating the same value over and over again. That costs performance and can be speed up using memoization. It simply stores the results of the recursive calls in an temporary array or other data structure. Whenever a recursive call needs the same result as the table has already stored, the result is going to be returned in a constant time instead of computing it again. This reduces the time complexity of the algorithm immense.

### 2. Properties of Dynamic programming

- Overlapping Subproblems $\rightarrow$ many computations are being calculated over and over again
- The problem can be divided into several subproblems which are dependent from each other but still distinct
- Each subproblem must produce a optimal solution to achieve an optimal solution for the optimal solution

### 3. Graphical merge sort

![Graphical Merge Sort](./w14_ex02/merge_sort.png)

### 4. Memoization cannot speed up divide-and-conquer algorithm

The big difference between dynamic programming and divide-and-conquer is that the problem is being divided into smaller subproblems but in DP it may be dependent and in divide-and-conquer it can be computed separately. The results of each subproblem may differ and therefore no overlapping subproblems may exist. Memoization is therefore useless in a divide-and-conquer algorithm as it won't speed it up.