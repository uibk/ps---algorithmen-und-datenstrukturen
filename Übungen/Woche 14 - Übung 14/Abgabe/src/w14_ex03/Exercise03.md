
Exercise 03
===

#### 1. Recursive definition for Fibonacci-Numbers

$$ F_n =
\left\{
    \begin{array}{l}
      0\\
      1\\
     F_{n-1} + F_{n-2}\\
    \end{array}
  \right.
    \begin{array}{l}
      n = 0\\
      n = 1\\
      n > 1\\
  \end{array}
  $$

### 2. Recursion tree

![Fibonacci Tree with $n = 6$](./w14_ex03/fibonacci_tree.png)

### 3. Dynamic Programming algorithm for Fibonacci Numbers

```Java
 static int fibonacci(int number) {
    int numbers[] = new int[number + 1];

    numbers[0] = 0; 
    numbers[1] = 1; 

    for (int i = 2; i < number + 1; i++) {
        numbers[i] = numbers[i-1] + numbers[i-2];
    }

    return numbers[number];
    }
```
