package w12_ex03;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(fibonacci(5));
    }

    static int fibonacci(int number) {
        int numbers[] = new int[number + 1];

        numbers[0] = 0;
        numbers[1] = 1;

        for (int i = 2; i <= number; i++) {
            numbers[i] = numbers[i - 1] + numbers[i - 2];
        }

        return numbers[number];
    }
}
