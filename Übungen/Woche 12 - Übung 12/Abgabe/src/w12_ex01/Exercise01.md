---
author: Andreas Murk
title: Algorithmen und Datenstrukturen Woche 13
description: Woche 13
---

### Exercise 01

![Directed Graph](./w12_ex01/adjacency_matrix.png){width=50%}

1. Check if all the nodes in the resulting graph are reachable from every node in maximum
3 steps.

No, not possible. Starting from edge $A \rightarrow F$ needs 4 steps.

2. What type of graph is it?

Directed cyclic graph

3. Does it have any cycles? If yes, write down the cycle.

Yes, there are several cycles:

$(A \rightarrow B \rightarrow A)$

$(A \rightarrow B \rightarrow C \rightarrow A)$

$(A \rightarrow D \rightarrow B \rightarrow C \rightarrow A)$

$(C \rightarrow A \rightarrow B \rightarrow A)$

$(C \rightarrow A \rightarrow D \rightarrow B \rightarrow C)$

$(D \rightarrow B \rightarrow C \rightarrow A \rightarrow D)$

$(E \rightarrow D \rightarrow B \rightarrow C \rightarrow E)$
