### Exercise 04

Adapted pseudo-code for negative edge weights. Similar to *Bell-Ford Algorithm*:


**Algorithm** BellFordShortestPaths(*list* vertices, *list* edges, *vertex* start):
// Distance from start vertex to itself is zero
Distance[start] $\leftarrow$ 0

// Fill all distances from vertices (except start vertex) to **infinity**
**foreach** vertex $v$ $\neq$ *start* in vertices **do**
Distance[$v$] $\leftarrow\inf$
prev[$v$] $\leftarrow$ nil

// Go trough **all** vertices
**from** 1 till size$(vertices) - 1$ **do**:
**for each** edge $(u, v)$ **with** weight $w$ in **edges**:
    **if** Distance[$u$] + $w$ < Distance[$v$]:
    Distance[$v$] = Distance[$u$] + $w$
    prev[v] = $u$

**return** Distance[], prev[]

The *Bell-Ford Algorithm* looks trough **all** vertices - 1 to get the distance for all given vertices so I can even detect negative weights of edges and process the distance further. By constrast, the *Dijkstra-Algorithm* simply looks trough **all** outgoing edges from the best vertex (which is the one with the lowest distance so far) and would never consider negative weigths in a graph.

##### 

Following graph is given:

![Directed Graph with cycle](./w12_ex04/bellman_ford.png)

When starting vertex $A$ to end-vertex $E$:
If he starts from $A$ it would be the shorter distance to vertex $C$ and then would choose vertex $F$ and then $D$. But instead of going to traverse our end-vertex $E$ it would again choose vertex $C$ as a shorter path has been found. This would end up in an infinite loop which finds an even shorter path every time.