package w12_ex02;


import java.util.ArrayList;
import java.util.List;

/**
 * Represents an edge in a given directed graph
 */
public class Edge<T> {

    private T element;
    private List<Vertex<T>> endpoints;
    private boolean isBackwardEdge;

    public Edge(Vertex<T> first, Vertex<T> second, T element) {
        this.element = element;
        endpoints = new ArrayList<>();
        endpoints.add(first);
        endpoints.add(second);
        isBackwardEdge = false;
    }

    public T getElement() {
        return element;
    }

    public boolean isBackwardEdge() {
        return isBackwardEdge;
    }

    public void setBackwardEdge(boolean backwardEdge) {
        isBackwardEdge = backwardEdge;
    }

    public List<Vertex<T>> getEndpoints() {
        return endpoints;
    }
}
