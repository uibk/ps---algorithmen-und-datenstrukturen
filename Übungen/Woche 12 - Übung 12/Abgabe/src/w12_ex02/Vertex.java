package w12_ex02;


import java.util.HashMap;

/**
 * This class represents a vertex for a directed graph implemented with a adjacency map
 */
public class Vertex<T> {

    private T element;
    private HashMap<Vertex<T>, Edge<T>> incomingEdges;
    private HashMap<Vertex<T>, Edge<T>> outgoingEdges;
    private boolean visited;
    private boolean doubleVisited;

    public Vertex(T element) {
        incomingEdges = new HashMap<>();
        outgoingEdges = new HashMap<>();
        this.visited = false;
        this.doubleVisited = false;
        this.element = element;
    }

    public HashMap<Vertex<T>, Edge<T>> getOutgoingEdges() {
        return outgoingEdges;
    }

    public HashMap<Vertex<T>, Edge<T>> getIncomingEdges() {
        return incomingEdges;
    }

    public T getElement() {
        return element;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isDoubleVisited() {
        return doubleVisited;
    }

    public void setDoubleVisited(boolean doubleVisited) {
        this.doubleVisited = doubleVisited;
    }

    @Override
    public String toString() {
        return "Vertex: " + getElement();
    }
}
