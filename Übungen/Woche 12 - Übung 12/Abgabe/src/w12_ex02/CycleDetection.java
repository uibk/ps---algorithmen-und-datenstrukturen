package w12_ex02;

import java.util.ArrayList;

public class CycleDetection {
    public static void main(String[] args) {
        AdjacencyMap<String> adjMap = new AdjacencyMap<>();

        Vertex a = adjMap.insertVertex("A");
        Vertex b = adjMap.insertVertex("B");
        Vertex c = adjMap.insertVertex("C");
        Vertex d = adjMap.insertVertex("D");
        Vertex e = adjMap.insertVertex("E");
        Vertex f = adjMap.insertVertex("F");

        adjMap.insertEdge(a, b, "AB");
        adjMap.insertEdge(a, d, "AD");
        adjMap.insertEdge(b, a, "BA");
        adjMap.insertEdge(b, c, "BC");
        adjMap.insertEdge(c, e, "CE");
        adjMap.insertEdge(c, a, "CA");
        adjMap.insertEdge(d, b, "DB");
        adjMap.insertEdge(e, d, "ED");
        adjMap.insertEdge(e, f, "EF");

        Detector<String> cd = adjMap.cycleDetector();
        ArrayList<Vertex<String>> finalList = cd.detectCycles(new ArrayList<>(), f);
        if (!finalList.isEmpty()) finalList.remove(finalList.size() - 1);
        System.out.println(finalList);
    }

}
