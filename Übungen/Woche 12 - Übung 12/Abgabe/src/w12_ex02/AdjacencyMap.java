package w12_ex02;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a directed graph implemented as an adjacency map
 */
public class AdjacencyMap<T> {

    private List<Vertex<T>> vertices;
    private List<Edge<T>> edges;
    private List<Vertex<T>> cycle;
    private boolean cycleDetected;

    public AdjacencyMap() {
        vertices = new LinkedList<>();
        edges = new LinkedList<>();
        cycle = new LinkedList<>();
        cycleDetected = false;
    }

    public Vertex insertVertex(T element) {
        Vertex<T> newVertex = new Vertex<>(element);
        vertices.add(newVertex);
        return newVertex;
    }

    public Edge insertEdge(Vertex<T> first, Vertex<T> second, T element) {
        if (getEdge(first, second) == null) {
            Edge<T> newEdge = new Edge<>(first, second, element);
            edges.add(newEdge);
            first.getOutgoingEdges().put(second, newEdge);
            second.getIncomingEdges().put(first, newEdge);
            return newEdge;
        } else {
            throw new IllegalArgumentException("Edge is existing already!");
        }
    }

    public int numVertices() {
        return vertices.size();
    }

    public List<Vertex<T>> vertices() {
        return vertices;
    }

    public int numEdges() {
        return edges.size();
    }

    public List<Edge<T>> edges() {
        return edges;
    }

    public Edge<T> getEdge(Vertex<T> first, Vertex<T> second) {
        return first.getOutgoingEdges().get(second);
    }

    public List<Vertex<T>> endVertices(Edge<T> e) {
        return e.getEndpoints();
    }

    public int outDegree(Vertex v) {
        return v.getOutgoingEdges().size();
    }

    public int inDegree(Vertex v) {
        return v.getIncomingEdges().size();
    }

    public Vertex<T> removeVertex(Vertex<T> v) {
        vertices.remove(v);
        return v;
    }

    public Edge<T> removeEdge(Edge<T> e) {
        edges.remove(e);
        return e;
    }

    public Vertex<T> opposite(Vertex<T> v, Edge<T> e) {
        return null;
    }

    public List<Vertex<T>> getCycle() {
        return cycle;
    }

    public Detector<T> cycleDetector() {
        return new CycleDetector();
    }

    private class CycleDetector implements Detector<T> {

        @Override
        public ArrayList<Vertex<T>> detectCycles(ArrayList<Vertex<T>> cycleList, Vertex<T> startVertex) {
            startVertex.setVisited(true);
            for (Vertex<T> outgoingVertex : startVertex.getOutgoingEdges().keySet()) {
                // for returning from foreach loop
                if (outgoingVertex.isVisited() && !cycleDetected) {
                    cycleDetected = true;
                    cycleList.add(startVertex);
                    cycleList.add(outgoingVertex);
                    outgoingVertex.setDoubleVisited(true);
                    return cycleList;
                } else if (!cycleDetected) {
                    detectCycles(cycleList, outgoingVertex);
                }
                // for coming back from recursion
                if (cycleDetected) {
                    if (!cycleList.contains(outgoingVertex)) {
                        cycleList.add(outgoingVertex);
                        if (outgoingVertex.isDoubleVisited()) cycleDetected = false;
                        return cycleList;
                    }
                }
            }
            return cycleList;
        }
    }
}
