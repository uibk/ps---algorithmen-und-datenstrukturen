package w12_ex02;

import java.util.ArrayList;

public interface Detector<T> {
    ArrayList<Vertex<T>> detectCycles(ArrayList<Vertex<T>> vertices, Vertex<T> startVertex);
}
