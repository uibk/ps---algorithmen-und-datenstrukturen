### Exercise 03

![Output from Breadth First Traversal](./w12_ex03/bfs_tree.png){width=50%}

1. List the nodes according to the access through the breadth-first search. Mark all the edges which are tree-edges.

2. Are there any backward-edges in the graph? If yes, mark them.

3. Are there any cross-edges in the graph? If yes, mark them.

Output: $\{ A, B, C, E, D, G, F\}$

Tree-edges are marked with straight blue lines. Backward-edges are marked in red color. Cross-edges are marked with dotted lines. 
