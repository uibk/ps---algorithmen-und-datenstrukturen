# Solutions Week 04

## Exercise 1: Recursion

Given a list of integers (e.g. `[0, 1, 1, 3, 5, 6, 1]`). One would like to know the number of occurrences for a given number.

1. Write an iterative function that counts the occurrences for a given number n in Java.

    **Hint:** parameters are the number to count $(n)$ and the list of integers.

```Java
private static int countOccurenceIterative(int n, int[] array) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (n == array[i]) {
                counter++;
            }
        }
        return counter;
    }
}
```

2. Transform the iterative function into a recursive function (again in Java).

    **Hint:** parameters are the number to count $(n)$ and the list of integers.

    **Sidenote:** *position* is a static data field

```Java
private static int countOccurenceRecursive(int n, int[] array) {
        if (position <= (array.length - 1)) {
            if (n == array[position]) {
                // Add 1 (similar to counter++)
                position++;
                return countOccurenceRecursive(n, array) + 1;
            } else {
                // Move to next index of array
                position++;
                return countOccurenceRecursive(n, array);
            }
        } else {
            return 0;
        }
    }
```

3. Determine the run time complexity for the recursive function using Big-O notation.

    The runtime complexity of the tail-recursive function is  $\mathcal{O}({n})$, because I iterate (recursively) trough the whole array once (until position is equal to array length - 1).

## Exercise 2: Call tree

The Tower of Hanoi consists of three stacks and a number of disks with a different size. The disks can be moved to any rod. The game starts with all disks on one stack in order of size, the smallest disk at the top. The idea is to move the tower from one stack to another, using one temporary stack.

Rules are the following:

1. Only one disk can be moved at a time.
2. At each move you can just take the upper disk and put it on top of one of the other stacks. 
3. No larger disk may be placed on a smaller disk.

The following recursive function solves this problem:

```python
def TowerOfHanoi(n, a, b, tmp):
        if n == 1:
            b.append(a.pop())
        else:
            TowerOfHanoi(n-1, a, tmp, b)
            b.append(a.pop())
            TowerOfHanoi(n-1, tmp, b, a)
stack1 = [3,2,1]
stack2 = []
stack3 = []
```

Your task is to determine and draw the recursion trace (Aufrufbaum) for the call

```python
# Move the tower from stack one to stack three with
# stack two as temporary stack
TowerOfHanoi(len(stack1), stack1, stack3, stack2)
```

<img src="/home/andreas-imst/Developing/PS - Algorithmen und Datenstrukturen/Übungen/Woche 03 - Übung 03/Abgabe/img/towerofhanoi.png">

**Hint:** The stacks are shared between the recursive calls. The pop function returns the last element of the given list. Append appends an element at the end of the list

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

## Exercise 3: Tail recursion

A farmer owns $60.000$ hectares of forest in year $0$. The forest grows $5$ percent every year and the farmer deforests $3500$ hectares per year. The following function expresses the given conditions.

$$ f(x) =
    \begin{cases} 
      60000 && \textrm{if}\ x = 0 \\
      1,05\cdot f(x-1)-3500 && \textrm{if}\ x>0
   	\end{cases}
$$

x defines the year. Year $0$ is the start and the next year would be year $1$ and so on.

1. What is a tail-recursive function?

    In a tail-recursive function you perform your calculations before the execution of the recursive function happens. The results then would be passed to the next step using the parameter.
    e.g.

```Java
public void int recursiveSum(int x, int sum) {
    if(x == 0) {
        return sum;
    } else {
        return recursiveSum(x - 1, sum + x);
    }
}
```  

2. Implement (in Java) a tail-recursive version of the function as defined above.

```Java
public class HectaresMain {

    public static void main(String[] args) {
        System.out.println(hectares(60000, 15));
    }

    public static double hectaresRecursive(double hectares, int years) {
        if (years > 0) {
            return hectaresRecursive((1.05 * hectares) - 3500, years - 1);
        }
        return hectares;
    }

    public static double hectares(double hectares, int years) {
        if (years == 0) {
            return hectares;
        } else {
            return hectaresRecursive(hectares, years);
        }
    }
}

```

<br/><br/>

## Exercise 4: Stacks and queues

1. **Stack**
Words like *anna* and *racecar* are so-called palindromes. Implement in Java a program that reads a word from the command line and puts it into a stack. The stack should be used to determine if a given word is a palindrome or not.

    **Hint:** you don’t need to implement a recursive function. Use a stack and the given word (input) to determine if the input is a palindrome.

```Java
class CharacterStack {

    private Character[] array;
    private int size;

    public CharacterStack(int capacity) {
        this.size = 0;
        this.array = new Character[capacity];
    }

    public void push(Character item) {
        if (size <= array.length) {
            array[(array.length - 1) - size] = item;
            size++;
        } else {
            System.err.println("CharacterStack already full!");
        }
    }

    public Character pop() {
        if (isEmpty()) {
            return null;
        }
        size--;
        return array[(array.length - 1) - size];
    }

    ...
}
```

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/>
```Java
import java.util.Scanner;

class PalindromeApplication {

    private Scanner scanner;
    private CharacterStack stack;

    public PalindromeApplication(
    Scanner scanner, CharacterStack stack) {
        this.scanner = scanner;
        this.stack = stack;
    }

    public String getUserInput() {
        System.out.println("Please type in your word:");
        return scanner.nextLine();
    }

    public boolean checkPalindrome(String word) {
        fillStack(word);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.stack.size(); i++) {
            stringBuilder.append(this.stack.pop());
        }
        return word.contentEquals(stringBuilder);
    }

    private void fillStack(String word) {
        for (int i = 0; i < word.length(); i++) {
            this.stack.push(word.toLowerCase().charAt(i));
        }
    }
}
```

```Java
import java.util.Scanner;

public class PalindromeMain {
    public static void main(String[] args) {
        PalindromeApplication pa = new PalindromeApplication(
        new Scanner(System.in),
        new CharacterStack(100));

        pa.checkPalindrome(pa.getUserInput()) ?
        System.out.println("Is a palindrome!") :
        System.out.println("Is not a palindrome!")));
    }
}
```

1. **Queue**
    Implement a queue in Java which holds integers. After that execute the following commands **in the order shown below** (the commands are described in pseudocode, please execute them in the main method of your java class). Submit your code and the output after executing the commands.

     |      |     command      |      |     command      |
     | :--- | :--------------: | :--- | :--------------: |
     | (a)  |    enqueue(5)    | (e)  |    enqueue(2)    |
     | (b)  |    enqueue(1)    | (f)  | print(dequeue()) |
     | (c)  |  print(first())  | (g)  | print(dequeue()) |
     | (d)  | print(dequeue()) | (h)  | print(isEmpty()) |

```Java
public class Queue {

    private Integer[] array;
    private int end;
    private int start;
    private int size;

    public Queue(int capacity) {
        this.array = new Integer[capacity];
        this.end = 0;
        this.start = 0;
        this.size = 0;
    }

    public void enqueue(Integer item) {
        if (size == array.length - 1) {
            System.err.println("Queue is full!");
        } else {
            array[end] = item;
            end = (end + 1) % (array.length);
        }
    }

    public Integer dequeue() {
        if (isEmpty()) {
            System.err.println("Queue is empty!");
            return null;
        } else {
            Integer item = array[start];
            start = (start + 1) % (array.length);
            return item;
        }
    }

    ...
}
```

<br/>

```Java
Output:
5
5
1
2
true
```