package ex04_02;

public class QueueMain {
    public static void main(String[] args) {
        Queue queue = new Queue(10);

        queue.enqueue(5);
        queue.enqueue(1);
        System.out.println(queue.first());
        System.out.println(queue.dequeue());
        queue.enqueue(2);
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.isEmpty());
    }
}
