package ex04_02;

/**
 * Own defined class Queue
 */
public class Queue {

    private Integer[] array;
    private int end;
    private int start;
    private int size;

    /**
     * Constructor for instantiating queues
     *
     * @param capacity size of array
     */
    public Queue(int capacity) {
        this.array = new Integer[capacity];
        this.end = 0;
        this.start = 0;
        this.size = 0;
    }

    /**
     * Returns number of elements in the queue
     *
     * @return size of consisting elements
     */
    int size() {
        return ((array.length) - start) + end;
    }

    /**
     * Tests if queue is empty
     *
     * @return true if queue is empty, false when not
     */
    boolean isEmpty() {
        return start == end;
    }

    /**
     * Inserts element at the end of the queue.
     *
     * @param item to be inserted
     */
    void enqueue(Integer item) {
        if (size == array.length - 1) {
            System.err.println("Queue is full!");
        } else {
            array[end] = item;
            end = (end + 1) % (array.length);
        }
    }

    /**
     * Removes element at the end of the queue.
     *
     * @return removed element (or null if empty)
     */
    Integer dequeue() {
        if (isEmpty()) {
            System.err.println("Queue is empty!");
            return null;
        } else {
            Integer item = array[start];
            start = (start + 1) % (array.length);
            return item;
        }
    }

    /**
     * Returns element at the beginning of the queue but does not remove it
     */
    Integer first() {
        if (isEmpty()) {
            System.err.println("Queue is empty!");
            return null;
        } else {
            return array[start];
        }
    }
}
