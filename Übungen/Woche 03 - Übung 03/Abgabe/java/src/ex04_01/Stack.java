package ex04_01;

/**
 * Own created class for Stack operations
 */
class Stack {

    private Character[] array;
    private int size;

    Stack(int capacity) {
        this.size = 0;
        this.array = new Character[capacity];
    }

    /**
     * Adds item to the beginning of the stack
     *
     * @param item the element to be inserted
     */
    void push(Character item) {
        if (size <= array.length) {
            array[(array.length - 1) - size] = item;
            size++;
        } else {
            System.err.println("Stack already full!");
        }
    }

    /**
     * Removes and returns an item from the top of the stack
     *
     * @return removed element (or null if empty)
     */
    Character pop() {
        if (isEmpty()) {
            return null;
        }
        size--;
        return array[(array.length - 1) - size];
    }

    /**
     * Returns element at the top of the stack but does not remove it
     *
     * @return top element in the stack (or null if empty)
     */
    Character top() {
        if (isEmpty()) {
            return null;
        }
        return array[(array.length - 1) - size];
    }

    /**
     * Tests if stack is empty
     *
     * @return either true if stack is empty or false when not
     */
    private boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns the number of elements in the stack.
     *
     * @return number of elements in the stack
     */
    int size() {
        return size;
    }
}
