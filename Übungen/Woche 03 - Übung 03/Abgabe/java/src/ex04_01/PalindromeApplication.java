package ex04_01;

import java.util.Scanner;

class PalindromeApplication {

    private Scanner scanner;
    private Stack stack;

    PalindromeApplication(Scanner scanner, Stack stack) {
        this.scanner = scanner;
        this.stack = stack;
    }

    public String getUserInput() {
        System.out.println("Please type in your word:");
        return scanner.nextLine();
    }

    boolean checkPalindrome(String word) {
        fillStack(word);
        StringBuilder stringBuilder = new StringBuilder();
        int index = this.stack.size();
        for (int i = 0; i < index; i++) {
            stringBuilder.append(this.stack.pop());
        }
        return word.contentEquals(stringBuilder);
    }

    private void fillStack(String word) {
        for (int i = 0; i < word.length(); i++) {
            this.stack.push(word.toLowerCase().charAt(i));
        }
    }
}
