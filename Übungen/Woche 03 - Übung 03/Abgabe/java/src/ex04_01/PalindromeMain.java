package ex04_01;

import java.util.Scanner;

public class PalindromeMain {
    public static void main(String[] args) {
        PalindromeApplication pa = new PalindromeApplication(new Scanner(System.in), new Stack(100));
        if (pa.checkPalindrome(pa.getUserInput())) {
            System.out.println("Is a palindrome!");
        } else {
            System.out.println("Is not a palindrome!");
        }
    }
}

