package ex01;

public class RecursionApplication {
    private static int position = 0;

    public static void main(String[] args) {
        int[] integerList = new int[7];
        integerList[0] = 0;
        integerList[1] = 1;
        integerList[2] = 1;
        integerList[3] = 3;
        integerList[4] = 5;
        integerList[5] = 6;
        integerList[6] = 1;
        System.out.println(countOccurenceIterative(5, integerList));
        System.out.println(countOccurenceRecursive(1, integerList, 0));
        System.out.println(countOccurenceRecursive(1, integerList));
    }

    private static int countOccurenceIterative(int n, int[] array) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (n == array[i]) {
                counter++;
            }
        }
        return counter;
    }

    // Solution with third parameter
    private static int countOccurenceRecursive(int n, int[] array, int position) {
        if (position <= (array.length - 1)) {
            if (n == array[position]) {
                // Add 1 (similar to counter++)
                return countOccurenceRecursive(n, array, position + 1) + 1;
            } else {
                // Move to next index of array
                return countOccurenceRecursive(n, array, position + 1);
            }
        } else {

            return 0;
        }
    }

    private static int countOccurenceRecursive(int n, int[] array) {
        if (position <= (array.length - 1)) {
            if (n == array[position]) {
                // Add 1 (similar to counter++)
                position++;
                return countOccurenceRecursive(n, array) + 1;
            } else {
                // Move to next index of array
                position++;
                return countOccurenceRecursive(n, array);
            }
        } else {

            return 0;
        }
    }
}