package ex03;

public class HectaresMain {

    public static void main(String[] args) {
        System.out.println(hectares(60000, 15));
    }

    public static double hectaresRecursive(double hectares, int years) {
        if (years > 0) {
            return hectaresRecursive((1.05 * hectares) - 3500, years - 1);
        }
        return hectares;
    }

    public static double hectares(double hectares, int years) {
        if (years == 0) {
            return hectares;
        } else {
            return hectaresRecursive(hectares, years);
        }
    }
}
