package w10_ex03;

import java.util.ArrayList;
import java.util.List;

public class FindMinimum {

    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(10);
        integerList.add(11);
        integerList.add(17);
        integerList.add(2);
        integerList.add(5);
        integerList.add(7);

        System.out.println(getMinimum(0, integerList.size() - 1, integerList));
    }

    public static Integer getMinimum(int min, int max, List<Integer> integerList) {
        if (integerList.isEmpty()) {
            throw new IllegalArgumentException("List is empty!");
        }

        if (min == max) {
            return integerList.get(min);
        }

        int middle = min + (max - min) / 2;

        if (integerList.get(middle) > integerList.get(max)) {
            return getMinimum(middle + 1, max, integerList);
        } else if (integerList.get(middle) < integerList.get(max)) {
            return getMinimum(min, middle, integerList);
        }
        return null;
    }
}
