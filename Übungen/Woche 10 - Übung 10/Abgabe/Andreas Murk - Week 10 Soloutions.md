### Andreas Murk - Week 11 Homework

##### Exercise 01

<img src="src/w10_ex01/redblacktree_insert_1.png">
<img src="src/w10_ex01/redblacktree_insert_2.png">
<img src="src/w10_ex01/redblacktree_insert_3.png">
<img src="src/w10_ex01/redblacktree_insert_4.png">
<img src="src/w10_ex01/redblacktree_insert_5.png">
<img src="src/w10_ex01/redblacktree_insert_6.png">
<img src="src/w10_ex01/redblacktree_insert_8.png">
<img src="src/w10_ex01/redblacktree_insert_12.png">
<img src="src/w10_ex01/redblacktree_insert_13.png">
<img src="src/w10_ex01/redblacktree_insert_14.png">
<img src="src/w10_ex01/redblacktree_insert_16.png">
<img src="src/w10_ex01/redblacktree_insert_17.png">

<br/>
<br/>
<br/>
<br/>

###### Exercise 02

The algorithm would be a modified merge-sort and would have a  complexity of $\mathcal{O}(n\ *\ log\ n)$. I hadn't had time to finally complete it but submitted an approach which would solve the problem using an divide and conquer algorithm (as like merge-sort does). 

###### Exercise 03

I am using a binary search algorithm and always compare the elements in the middle and at the end as we can assure that highest value is in the middle and lower values are from middle to the end of the array. It has a complexity of $\mathcal{O}(log\ n)$ as a normal binary-search algorithm.