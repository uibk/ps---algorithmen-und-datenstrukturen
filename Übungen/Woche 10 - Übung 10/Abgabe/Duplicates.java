package w10_ex01;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Duplicates {

    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(5);
        integerList.add(3);
        integerList.add(3);
        integerList.add(2);
        integerList.add(1);
        integerList.add(3);

        removeDuplicates(integerList);
    }

    public static void removeDuplicates(List<Integer> integerList) {
        int n = integerList.size();
        if (n < 2) {
            return;
        }
        int middle = n / 2; // integer division

        List<Integer> s1 = integerList.subList(0, middle);
        List<Integer> s2 = integerList.subList(middle, n);
        removeDuplicates(s1);
        removeDuplicates(s2);
        System.out.println(merge(s1, s2, integerList));
    }

    private static List<Integer> merge(List<Integer> s1, List<Integer> s2, List<Integer> originaList) {
        int i = 0, j = 0;
        List<Integer> finalList = new ArrayList<>();
        while (i < s1.size() && j < s2.size()) {
            if (!s1.get(i).equals(s2.get(j))) {
                finalList.add(s1.get(i++));
            } else if (!finalList.isEmpty() && !finalList.get(i).equals(s2.get(j))) {
                finalList.add(s2.get(j++));
            }
        }
        return originaList;
    }
}

