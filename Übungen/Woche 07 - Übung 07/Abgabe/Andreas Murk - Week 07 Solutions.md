### Andreas Murk - Week 07

#### Exercise 01

##### (a) Hash-Function
```
l = length of word
sA = ascii value of second letter
fA = ascii value of first letter

h(k) = (sA * l) * (fA * l) mod 11
```

$$1.\ Algorithm \ (108 \mathcal\ {* \ 9} )\ \mathcal{*} \ (65 \ \mathcal{*} \ 9) = 568620 \ mod \ 11 = 8$$

$$2.\ Datastructure \ (97 \mathcal\ {* \ 13} )\ \mathcal{*} \ (68 \ \mathcal{*} 13) = 1114724 \ mod \ 11 = 6$$

$$3.\ Proseminar \ (114 \mathcal\ {* \ 10} )\ \mathcal{*} \ (80 \ \mathcal{*} 10) = 912000 \ mod \ 11 = 1$$

$$4.\ Exercisesheet \ (120 \mathcal\ {* \ 13} )\ \mathcal{*} \ (69 \ \mathcal{*} 13) = 1399320 \ mod \ 11 = 10$$

##### (b) Problems with simple Hash-Functions

This simple hash function would result in many collisions because many words could have the same length, ascii value for the second and first letter. The implementation could either override and replace the words in an index-based cell. 
Apart from that, the function is easy predictable for a hacker and therefore creates a huge lack of security.

#### Exercise 02

Text would be too large for file. Therefore please use the console output of the provided file(s).

#### Exercise 03

* Word length of 2 [in, zu, in, zu]
* Word length of 3 [und, und, und, und, der, und, und, sie, ein, für, die, der]
* Word length of 4 [sind, Lage, sich]
* Word length of 5 [haben]
* Word length of 6 [dieses, Moduls, kennen]
* Word length of 7 [weitere, eigenen, Weiters]
* Word length of 8 [wichtige]
* Word length of 9 [verstehen, verwenden]
* Word length of 10 [Programmen]
* Word length of 11 [Absolventen, Algorithmen, Algorithmen, erschließen, Verständnis, Komplexität, Algorithmen]
* Word length of 13 [selbstständig, verschiedenen]
* Word length of 14 [Absolventinnen]
* Word length of 15 [Datenstrukturen, Datenstrukturen]

#### Exercise 04

##### Collision handling

<img src="src/w04_ex04/Collision_handling.png">