package w07_ex02;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniversityMain {
    public static void main(String[] args) {

        List<String> financeEssentialsStudents = new ArrayList<>();
        List<String> riskManagementEssentialsStudents = new ArrayList<>();
        List<String> enterPriseResourceStudents = new ArrayList<>();
        List<String> trophicalInteractionsStudents = new ArrayList<>();
        List<String> metaBarcodingStudents = new ArrayList<>();
        List<String> communityInterpretingStudents = new ArrayList<>();
        List<String> bibliothecalPracticsStudents = new ArrayList<>();

        fillLists(financeEssentialsStudents,
                riskManagementEssentialsStudents,
                enterPriseResourceStudents,
                trophicalInteractionsStudents,
                metaBarcodingStudents,
                communityInterpretingStudents,
                bibliothecalPracticsStudents);

        HashMap<String, List<String>> institute1Map = createInstitute1Map(financeEssentialsStudents, riskManagementEssentialsStudents, enterPriseResourceStudents);
        HashMap<String, List<String>> institute2Map = createInstitute2Map(trophicalInteractionsStudents, metaBarcodingStudents);
        HashMap<String, List<String>> institute3Map = createInstitute3Map(communityInterpretingStudents, bibliothecalPracticsStudents);

        University university = new University();

        printMap(university.intersect(institute1Map, institute2Map), "Intersection", "Institute 1", "Institute 2");

        printMap(university.union(institute2Map, institute3Map), "Union", "Institute 2", "Institute 3");

        printMap(university.difference(institute3Map, institute2Map), "Difference", "Institute 3", "Institute 2");
    }

    private static HashMap<String, List<String>> createInstitute3Map(List<String> communityInterpretingStudents, List<String> bibliothecalPracticsStudents) {
        HashMap<String, List<String>> institute3Map = new HashMap<>();
        institute3Map.put("Community Interpreting", communityInterpretingStudents);
        institute3Map.put("Regionale Einführung in die Bibliothekspraxis", bibliothecalPracticsStudents);
        return institute3Map;
    }

    private static HashMap<String, List<String>> createInstitute2Map(List<String> trophicalInteractionsStudents, List<String> metaBarcodingStudents) {
        HashMap<String, List<String>> institute2Map = new HashMap<>();
        institute2Map.put("Molekulare Analyse von trophischen Interaktion", trophicalInteractionsStudents);
        institute2Map.put("Metabarcodierung von trophischen Interaktionen", metaBarcodingStudents);
        return institute2Map;
    }

    private static HashMap<String, List<String>> createInstitute1Map(List<String> financeEssentialsStudents, List<String> riskManagementEssentialsStudents, List<String> enterPriseResourceStudents) {
        HashMap<String, List<String>> institute1Map = new HashMap<>();
        institute1Map.put("Finance Essentials", financeEssentialsStudents);
        institute1Map.put("Risk Management Essentials", riskManagementEssentialsStudents);
        institute1Map.put("Training in Enterprise Resource Planning in 10 Days", enterPriseResourceStudents);
        return institute1Map;
    }

    private static void fillLists(List<String> financeEssentialsStudents, List<String> riskManagementEssentialsStudents, List<String> enterPriseResourceStudents, List<String> trophicalInteractionsStudents, List<String> metaBarcodingStudents, List<String> communityInterpretingStudents, List<String> bibliothecalPracticsStudents) {
        financeEssentialsStudents.add("Dierdre Palomares");
        financeEssentialsStudents.add("Alvaro Leon");
        financeEssentialsStudents.add("Gustavo Osborn");
        financeEssentialsStudents.add("Laurinda Wahlstrom");
        financeEssentialsStudents.add("Murray Mccall");

        riskManagementEssentialsStudents.add("Alishia Dunmore");
        riskManagementEssentialsStudents.add("Vannesa Poythress");
        riskManagementEssentialsStudents.add("Dario Stillings");
        riskManagementEssentialsStudents.add("Glady Juarbe");
        riskManagementEssentialsStudents.add("Murray Mccall");

        enterPriseResourceStudents.add("Tenisha Tinsley");
        enterPriseResourceStudents.add("Jenette Palacios");
        enterPriseResourceStudents.add("Ervin Hamil");
        enterPriseResourceStudents.add("Germaine Wohl");
        enterPriseResourceStudents.add("Esther Strohm");
        enterPriseResourceStudents.add("Leona Tippie");

        trophicalInteractionsStudents.add("Glady Juarbe");
        trophicalInteractionsStudents.add("Gustavo Osborn");
        trophicalInteractionsStudents.add("Laurinda Wahlstrom");

        metaBarcodingStudents.add("Alishia Dunmore");
        metaBarcodingStudents.add("Alvaro Leon");
        metaBarcodingStudents.add("Glady Juarbe");

        communityInterpretingStudents.add("Dierdre Palomares");
        communityInterpretingStudents.add("Alvaro Leon");
        communityInterpretingStudents.add("Gustavo Osborn");
        communityInterpretingStudents.add("Laurinda Wahlstrom");
        communityInterpretingStudents.add("Murray Mccall");

        bibliothecalPracticsStudents.add("Alishia Dunmore");
        bibliothecalPracticsStudents.add("Vannesa Poythress");
        bibliothecalPracticsStudents.add("Dario Stillings");
        bibliothecalPracticsStudents.add("Glady Juarbe");
    }

    private static void printMap(Map<String, List<String>> hashMap, String mapType, String firstMap, String secondMap) {
        System.out.println(mapType + " of " + firstMap + " and " + secondMap + ":");
        hashMap.forEach((student, courses) -> System.out.println("Student: [" + student + "] Courses: " + courses));
        System.out.println();
    }
}
