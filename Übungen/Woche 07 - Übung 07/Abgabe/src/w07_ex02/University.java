package w07_ex02;

import java.util.*;

public class University {

    public University() {
    }

    public Map<String, List<String>> intersect(HashMap<String, List<String>> institute1, HashMap<String, List<String>> institute2) {
        Map<String, List<String>> intersectMap = new HashMap<>();
        Map<String, List<String>> finalMap = new HashMap<>();

        // Fill intersection map with students as keys from institute 1
        institute1.forEach((course, students) -> students.forEach((student) -> {
            // If student is already existing, he subscribed to more than one course -> just add course
            if (intersectMap.containsKey(student)) {
                List<String> courses = intersectMap.get(student);
                courses.add(course);
            } else {
                // If student has not been stored into hashmap -> store whole student with course to hashmap
                List<String> courses = new LinkedList<>();
                courses.add(course);
                intersectMap.put(student, courses);
            }
        }));

        institute2.forEach((course, students) -> students.forEach((student) -> {
            // Check if student is stored as key in intersection map
            if (intersectMap.containsKey(student)) {
                // Same approach like institute 1 (student is existing, only add additional course(s)
                if (finalMap.containsKey(student)) {
                    List<String> courses = intersectMap.get(student);
                    courses.add(course);
                    // Otherwise get current course list from given student and add additional course(s)
                    // -> place student with all courses into final intersection map
                } else {
                    List<String> courses = intersectMap.get(student);
                    courses.add(course);
                    finalMap.put(student, courses);
                }
            }
        }));
        return finalMap;
    }

    public Map<String, List<String>> union
            (HashMap<String, List<String>> institute1, HashMap<String, List<String>> institute2) {
        Map<String, List<String>> unionMap = new HashMap<>();

        institute1.forEach((course, students) -> students.forEach((student) -> {
            List<String> courseList = new LinkedList<>();
            courseList.add(course);
            unionMap.put(student, courseList);
        }));

        institute2.forEach((course, students) -> students.forEach((student) -> {
            // If student already stored just add additional course
            if (unionMap.containsKey(student)) {
                List<String> courses = unionMap.get(student);
                courses.add(course);
            } else {
                List<String> courses = new LinkedList<>();
                courses.add(course);
                unionMap.put(student, courses);
            }
        }));
        return unionMap;
    }

    public Map<String, List<String>> difference
            (HashMap<String, List<String>> institute1, HashMap<String, List<String>> institute2) {
        Map<String, List<String>> differenceMap = new HashMap<>();

        // Fill difference map with students as keys from institute 1
        institute1.forEach((course, students) -> students.forEach((student) -> {
            List<String> courseList = new ArrayList<>();
            courseList.add(course);
            differenceMap.put(student, courseList);
        }));

        institute2.forEach((course, students) ->
                // Remove all students where name is equal to institute 2 course student name
                students.forEach(differenceMap::remove));
        return differenceMap;
    }
}
