package w07_ex03;

import java.util.*;

public class Multiset {

    private static String textContainer = "Absolventen und Absolventinnen dieses Moduls kennen " +
            "und verstehen wichtige Algorithmen und Datenstrukturen, " +
            "und sind in der Lage, sich weitere Algorithmen und " +
            "Datenstrukturen selbstständig zu erschließen, und " +
            "in eigenen Programmen zu verwenden. Weiters haben " +
            "sie ein Verständnis für die Komplexität der " +
            "verschiedenen Algorithmen.";

    private static List<String> textCollection = Arrays.asList(textContainer.replace(",", "").replace(".", "").split(" "));

    public HashMap<Integer, List<String>> countWordsLength() {
        HashMap<Integer, List<String>> hashMap = new HashMap<>();
        textCollection.forEach((word) -> {
            List<String> wordList = new LinkedList<>();
            textCollection.forEach((w -> {
                if (word.length() == w.length()) {
                    wordList.add(w);
                }
            }));
            hashMap.put(word.length(), wordList);
        });
        return hashMap;
    }

    public void printWordsLength(HashMap<Integer, List<String>> hashMap) {
        hashMap.forEach((wordLength, wordList) -> System.out.println("Word length of " + wordLength + " " + wordList));
    }
}
