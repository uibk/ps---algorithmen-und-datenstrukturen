#### Exercise 01

##### (a) Hash-Function
```
l = length of word
sA = ascii value of second letter
fA = ascii value of first letter

h(k) = (sA * l) * (fA * l) mod 11

```

##### (b) Problems with simple Hash-Functions

The mod operation basically defines the size of the array. 
In this particular example we are always shifting the element which causes a collision to the next higher index, otherwise to the front of the array.
When the array is full, we might get into troubles due to additional growing functions and higher runtime-complexities. 
