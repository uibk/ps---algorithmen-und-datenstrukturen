#### Exercise 03

Consider following array, which will be merge-sorted:


<img src="merge_sort.png">

As we already know, merge sort creates a binary tree of height which is proportional to $log\ n$  (height-criteria of binary search tree)

We have a maximum amount of divisions (and therefore subtrees) of $x$. We split our array $x$ times. The recursion depth of merge-sort is proportional to the amount of divisions (and therefore the height of the tree), so:

$$n = 2^x \rightarrow x = \mathcal{log_2(n)} \isin \mathcal{O}(log_2n)$$

So for $x$ divisions we need $log_2{n}$ recursive calls to split our whole array (or tree) into half. The recursion depth is always proportional to the recursive calls of the function. The merging of the array is not considered here.