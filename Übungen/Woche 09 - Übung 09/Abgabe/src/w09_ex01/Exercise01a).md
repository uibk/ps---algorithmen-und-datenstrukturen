##### Exercise 01

###### a)

To get the maximum height of an AVL-tree we need the minimum amount of nodes in each level. The number of internal nodes in an AVL-tree are growing exponentially to $h$:

$$n(1) = 1$$

$$n(2) = 2$$

Assumption: If only one node (root) is in the tree, the height is $h = 0$.

For $h \ge 3$:

$$n(h) = 1 + n(h - 1) + n(h - 2)$$

Assuming that $h = 3$ we can say that:

$$n(3) = 1 + n(2) + n(1) = 1\ + 2 + 1 = 4$$

We need at least $4$ nodes for the given height $h = 3$.

$$n(4) = 1 + n(3) + n(2) = 1\ + 2^2 + 2 = 1\ + 4 + 2 = 7$$

We can conclude that we need a height of $h = 4$ for at least $7$ nodes.

<br/>
<br/>
<br/>

###### b)

<img src="avl_insertion_1.png">
<img src="avl_insertion_2.png">
<img src="avl_insertion_3.png">
<img src="avl_insertion_4.png">