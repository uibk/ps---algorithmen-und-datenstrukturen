#### Exercise 04

###### Insertion-Sort

- no parallelism possible

###### Selection-Sort

- no parallelism possible

###### Quicksort

- getting the pivot element of arrray (sub-array), place all elements to either left- or right-side would be one well-defined subtask 
- this lasts until every array contains exactly one element -> for each up-sort in sub-array we take one subtask

##### Heap-Sort

- **Assumption**: bottom up heap-construction is needed for heap sort.
- heap-construction could be one subtask and corresponding up- and down-bubbles as subtasks as well
- once created, one subtask could be taking the minimum of the heap for every element
- placing the last inserted node into the root node and every down-bubble would represent another subtask

##### Merge-Sort

- You repeatedly split the sublists down to the point where you have single-element arrays.
- Then merge these in parallel back up to the processing tree until you obtain the fully merged array at the top of the tree.  

See: <a href="http://penguin.ewu.edu/~trolfe/ParallelMerge/ParallelMerge.html">Parallel Merge Sort Implementation</a>