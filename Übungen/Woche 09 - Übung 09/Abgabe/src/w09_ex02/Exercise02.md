#### Exercise 02

The algorithm looks at each job and their corresponding profit. It picks the 3 highest profits and then insert the appropriate job into the timeslot.

1. Job 2 -> Profit 200, done at 1
2. Job 5 -> Profit 150, done at 2
3. Job 3 -> Profit 100, done at 3

The greedy algorithm gets the optimal solution as the sumation of the 3 highest profits is $200 + 150 +100 = 450$.

The asymptotic growth rate can be represented as $\mathcal{O}(n²)$ because we need to iterate trough all profits and compare each to the profits of all other jobs:

$$f(n) = \sum_{i = 0}^{n} i = n\ \mathcal{*} \frac{(n + 1)}{2} \rightarrow \frac{n² + n}{2} \isin \mathcal{O}(n^2)$$

When all jobs have the **same profit**, we cannot yield an optimal solution because there doesn't exist one. As well as same deadlines of 1. If each job **must** be done at Deadline 1, only one job with the highest profit can take place in the timeslot and all others would be forgotten.