### Andreas Murk - Week 10

##### Exercise 01

###### a)

To get the maximum height of an AVL-tree we need the minimum amount of nodes in each level. The number of internal nodes in an AVL-tree are growing exponentially to $h$:

$n(1) = 1$

$n(2) = 2$

<br/> 

**Assumption**: If only one node (root) is in the tree, the height is $h = 0$.

<br/> 

For $h \ge 3$:

<br/>

$$n(h) = 1 + n(h - 1) + n(h - 2)$$

<br/>

Assuming that $h = 3$ we can say that:

<br/>

$$n(3) = 1 + n(2) + n(1) = 1\ + 2 + 1 = 4$$

<br/>

We need at least $4$ nodes for the given height $h = 3$.

<br/>

$$n(4) = 1 + n(3) + n(2) = 1\ + 2^2 + 2 = 1\ + 4 + 2 = 7$$

<br/>

We can conclude that we need a height of $h = 4$ for at least $7$ nodes.

<img src="ex01/avl_insertion_1.png">
<img src="ex01/avl_insertion_2.png">
<img src="ex01/avl_insertion_3.png">
<img src="ex01/avl_insertion_4.png">

#### Exercise 02

The algorithm looks at each job and their corresponding profit. It picks the 3 highest profits and then insert the appropriate job into the timeslot.

1. Job 2 -> Profit 200, done at 1
2. Job 5 -> Profit 150, done at 2
3. Job 3 -> Profit 100, done at 3

The greedy algorithm gets the optimal solution as the sumation of the 3 highest profits is $200 + 150 +100 = 450$.

The asymptotic growth rate can be represented as $\mathcal{O}(n²)$ because we need to iterate trough all profits and compare each to the profits of all other jobs:

$$f(n) = \sum_{i = 0}^{n} i = n\ \mathcal{*} \frac{(n + 1)}{2} \rightarrow \frac{n² + n}{2} \isin \mathcal{O}(n^2)$$

When all jobs have the **same profit**, we cannot yield an optimal solution because there doesn't exist one. As well as same deadlines of 1. If each job **must** be done at Deadline 1, only one job with the highest profit can take place in the timeslot and all others would be forgotten.

#### Exercise 03

Consider following array, which will be merge-sorted:


<img src="ex03/merge_sort.png">

As we already know, merge sort creates a binary tree of height which is proportional to $log\ n$  (height-criteria of binary search tree)

We have a maximum amount of divisions (and therefore subtrees) of $x$. We split our array $x$ times. The recursion depth of merge-sort is proportional to the amount of divisions (and therefore the height of the tree), so:

$$n = 2^x \rightarrow x = \mathcal{log_2(n)} \isin \mathcal{O}(log_2n)$$

So for $x$ divisions we need $log_2{n}$ recursive calls to split our whole array (or tree) into half. The recursion depth is always proportional to the recursive calls of the function. The merging of the array is not considered here.

<br/>
<br/>
<br/>

#### Exercise 04

###### Insertion-Sort

I think, it is theoretically not possible. But if, the approach would be the following:

- You can split getting the next element of the array into one subtask and sorting it correctly into the array (process A needs to send returned, next element to process B)

###### Selection-Sort

Again, I don't think it is possible, due to the fact that the sorting array always looks at one element at a time.

- Getting the smallest value (which is not already sorted) would be one subtask
- Sorting this passed element (next smallest one) would be then sorted in by another process

###### Quicksort

- We randomly choose pivot from one process and broad it to every process
- Each process divides its unsorted list into two lists: those smaller ones than the pivot and the greaters ones than the pivot
- Each process in the upper half of the process list send its list containg one element to a partner process in the lower half of the process list and receives a "high list" in return
- The upper-half process now has only values greater than the pivot and the lower-half process only smaller ones
- Process divide themselves into two groups and the algorithm recurses
- After $log n$ recursions every process has an unsorted list of values completely independent from the other processes

See: <a href="https://www.uio.no/studier/emner/matnat/ifi/INF3380/v10/undervisningsmateriale/inf3380-week12.pdf">Parallel quicksort algorithm</a>

###### Heap-Sort

- **Assumption**: Bottom up heap-construction is required for heap sort.
- heap-construction could be one subtask and corresponding up- and down-bubbles as subtasks as well
- once created, one subtask could be taking the minimum of the heap for every element
- placing the last inserted node into the root node and every down-bubble would represent another subtask

###### Merge-Sort

- You repeatedly split the sublists down to the point where you have single-element arrays.
- Then merge these in parallel back up to the processing tree until you obtain the fully merged array at the top of the tree.  

See: <a href="http://penguin.ewu.edu/~trolfe/ParallelMerge/ParallelMerge.html">Parallel Merge Sort Implementation</a>