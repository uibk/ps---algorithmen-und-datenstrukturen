# Andreas Murk - Week 06

| Operation
| ---------------------------------- | 
| 1. root = addRoot(22);             | 
| 2. p2 = addLeft(root, 10);         |  
| 3. p3 = addRight(p2, 15);   |
| 4. p4 = addLeft(p2, 9);           | 
| 5. p5 = addLeft(p4, 3);        |
| 6. p6 = addRight(root, 45); | 
| 7. p7 = addRight(p5, 4); |
| 8. p8 = addRight(p6, 31); |
| 9. p9 = addRight(p3, 12); |

<img src="binary_search_tree.png" style="margin-left: 110px">

### Complexity of finding a number $\mathcal{n}$ in a binary search tree

The worst-case complexity of a fully balanced binary tree (which is **not** the case in this example) for finding a number would normally be $\mathcal{O}(\mathcal{log}(n))$ due to halving the trees or sub-trees whenever a value is greater or lesser than the searched one. In this particular case we sometimes have not exactly 2 children in each node. So the worst-case complexity would be rather $\mathcal{O}(n)$ because the height and corresponding depth of the tree cannot be stated as $\mathcal{O}(log(n))$ when it is not fully balanced.