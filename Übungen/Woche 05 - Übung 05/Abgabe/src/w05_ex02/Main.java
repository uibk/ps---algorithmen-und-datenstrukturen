package w05_ex02;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree(22);

        fillBinaryTree(tree);

        Iterator<Node> breadthFirstIterator = tree.breadthFirstIterator();
        Iterator<Node> depthFirstIterator = tree.depthFirstIterator();

        System.out.println();
        System.out.println("Breadth First Traversing\n");

        while (breadthFirstIterator.hasNext()) {
            Node currentNode = breadthFirstIterator.next();
            System.out.println(currentNode.getValue() + " has been visited!");
        }
        System.out.println();
        System.out.println("Depth First Traversing\n");

        while (depthFirstIterator.hasNext()) {
            Node currentNode = depthFirstIterator.next();
            System.out.println(currentNode.getValue() + " has been visited!");
        }
    }

    private static void fillBinaryTree(BinaryTree tree) {
        int[] ints = {10, 15, 9, 3, 45, 4, 31, 12};

        for (int n : ints) {
            tree.add(n);
        }
    }
}
