package w05_ex02;

import w05_ex03.Queue;
import w05_ex03.Stack;

import java.util.Iterator;

public class BinaryTree {

    private Node root;
    private int size;

    public BinaryTree(int value) {
        size = 0;
        addRoot(value);
    }

    public Node root() {
        return root;
    }

    private int size() {
        return size;
    }

    private void addRoot(Integer value) {
        size++;
        root = new Node(value, null, null);
    }

    public Node[] children(Node node) {
        Node[] nodes = new Node[2];
        if (node.getLeftChild() != null) {
            nodes[0] = node.getLeftChild();
        }
        if (node.getRightChild() != null) {
            nodes[1] = node.getRightChild();
        }
        return nodes;
    }

    public Node add(int value) {
        Node currentNode = root;

        while (currentNode != null) {
            if (value > currentNode.getValue()) {
                if (currentNode.hasRightChild()) {
                    currentNode = currentNode.getRightChild();
                } else {
                    return addRight(currentNode, value);
                }
            } else {
                if (currentNode.hasLeftChild()) {
                    currentNode = currentNode.getLeftChild();
                } else {
                    return addLeft(currentNode, value);
                }
            }
        }
        return null;
    }

    private Node addLeft(Node node, int value) {
        size++;
        Node newNode = new Node(value, null, null);
        node.setLeftChild(newNode);
        return newNode;
    }

    private Node addRight(Node node, int value) {
        size++;
        Node newNode = new Node(value, null, null);
        node.setRightChild(newNode);
        return newNode;
    }

    public DepthFirstIterator depthFirstIterator() {
        return new DepthFirstIterator(new Stack(size));
    }

    public BreadthFirstIterator breadthFirstIterator() {
        return new BreadthFirstIterator(new Queue(size));
    }

    private abstract class GeneralIterator implements Iterator<Node> {
        Node[] nodes;
        private int currentIndex = 0;

        GeneralIterator() {
            nodes = new Node[size];
        }

        abstract void init();

        @Override
        public boolean hasNext() {
            return nodes.length > currentIndex;
        }

        @Override
        public Node next() {
            currentIndex++;
            return nodes[currentIndex - 1];
        }
    }


    private class BreadthFirstIterator extends GeneralIterator {
        private Queue queue;

        private BreadthFirstIterator(Queue queue) {
            this.queue = queue;
            init();
        }

        @Override
        void init() {
            queue.enqueue(root);
            fill();
        }

        private void fill() {
            int i = 0;
            while (!queue.isEmpty()) {
                Node currentNode = queue.dequeue();
                nodes[i] = currentNode;
                for (Node child :
                        children(currentNode)) {
                    if (child != null) {
                        queue.enqueue(child);
                    }
                }
                i++;
            }
        }
    }

    private class DepthFirstIterator extends GeneralIterator {
        private Stack stack;

        DepthFirstIterator(Stack stack) {
            this.stack = stack;
            init();
        }

        @Override
        void init() {
            stack.push(root);
            fill();
        }

        private void fill() {
            int i = 0;

            while (!stack.empty()) {
                Node newNode = stack.pop();
                nodes[i] = newNode;

                if (newNode.hasRightChild()) {
                    stack.push(newNode.getRightChild());
                }

                if (newNode.hasLeftChild()) {
                    stack.push(newNode.getLeftChild());
                }
                i++;
            }
        }
    }

}
