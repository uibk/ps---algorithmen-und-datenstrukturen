package w05_ex03;

import w05_ex02.Node;

/**
 * Own created class for Stack operations
 */
public class Stack {

    public static Node[] array;
    private int size;
    private int length;

    public Stack(int capacity) {
        this.size = 0;
        array = new Node[capacity];
        length = array.length - 1;
    }

    public Node[] getArray() {
        return array;
    }

    /**
     * Adds item to the beginning of the stack
     *
     * @param item the element to be inserted
     */
    public void push(Node item) {
        if (size <= array.length) {
            array[(length) - size] = item;
            size++;
        } else {
            System.err.println("Stack already full!");
        }
    }

    /**
     * Removes and returns an item from the top of the stack
     *
     * @return removed element (or null if empty)
     */
    public Node pop() {
        if (isEmpty()) {
            return null;
        }
        size--;
        return array[(length) - size];
    }

    /**
     * Returns element at the top of the stack but does not remove it
     *
     * @return top element in the stack (or null if empty)
     */
    Node top() {
        if (isEmpty()) {
            return null;
        }
        return array[(length) - size];
    }

    /**
     * Tests if stack is empty
     *
     * @return either true if stack is empty or false when not
     */
    private boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns the number of elements in the stack.
     *
     * @return number of elements in the stack
     */
    int size() {
        return size;
    }

    public boolean empty() {
        return size() == 0;
    }
}
