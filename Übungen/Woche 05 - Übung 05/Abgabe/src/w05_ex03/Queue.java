package w05_ex03;


import w05_ex02.Node;

/**
 * Own defined class Queue
 */
public class Queue {

    private Node[] array;
    private int end;
    private int start;
    private int size;

    /**
     * Constructor for instantiating queues
     *
     * @param capacity size of array
     */
    public Queue(int capacity) {
        this.array = new Node[capacity];
        this.end = 0;
        this.start = 0;
        this.size = 0;
    }

    /**
     * Returns number of elements in the queue
     *
     * @return size of consisting elements
     */
    int size() {
        return ((array.length) - start) + end;
    }

    /**
     * Tests if queue is empty
     *
     * @return true if queue is empty, false when not
     */
    public boolean isEmpty() {
        return end == start;
    }

    /**
     * Inserts element at the end of the queue.
     *
     * @param item to be inserted
     */
    public void enqueue(Node item) {
        if (size == array.length - 1) {
            System.err.println("Queue is full!");
        } else {
            array[end] = item;
            end = (end + 1) % (array.length);
        }
    }

    /**
     * Removes element at the end of the queue.
     *
     * @return removed element (or null if empty)
     */
    public Node dequeue() {
        if (isEmpty()) {
            System.err.println("Queue is empty!");
            return null;
        } else {
            Node item = array[start];
            start = (start + 1) % (array.length);
            return item;
        }
    }

    /**
     * Returns element at the beginning of the queue but does not remove it
     */
    public Node first() {
        if (isEmpty()) {
            System.err.println("Queue is empty!");
            return null;
        } else {
            return array[start];
        }
    }
}
