# Week 05 - Andreas Murk

## Exercise 01

| Operation                          | Result                        |
| ---------------------------------- | ----------------------------- |
| 1. L.addFirst(100000)              | list = [0]                    |
| 2. L.addLast(8)                    | list = [0, 8]                 |
| 3. p = L.addBefore(L.last(), 1)    | list = [0, 1$_p$, 8]          |
| 4. L.addBefore(p, 7)               | list = [0, 7, 1$_p$, 8]       |
| 5. L.addAfter(L.first(), 5)        | list = [0 ,5 , 7, 1$_p$, 8]   |
| 6. L.addAfter(L.first(), 8)        | list = [0, 8, 5, 7, 1$_p$, 8] |
| 7. L.remove(L.before(L.before(p))) | list = [0, 8, 7, 1$_p$, 8]    |

## Exercise 02

```Java
public static void LRU(LinkedPositionalList<Integer> list, Integer item) {
        Position tmp = getExistingPosition(list, item);
        if (tmp == null) {
            list.addFirst(item);
        } else {
            list.remove(tmp);
            list.addFirst(item);
        }
    }
```

```Java
private static Position getExistingPosition(
LinkedPositionalList<Integer> list, Integer item) {
        for (Position p : list.positions()) {
            if (p.getElement().equals(item)) {
                return p;
            }
        }
        return null;
    }
```

<br/><br/><br/>

##### 2. 

In the following results I am explicitly talking about complexities from the static $LRU$ function and their operations shown in the first codeblock of this section.

The worst-case of inserting elements occurs whether an element with the same value is already stored at the end of the list or a new element should be inserted.
We need to iterate trough all positions to determine the exact position of the element or if a position has already got an element with the same value. It ends up in a complexity of O(n).  

##### 3. 

The execution time of this list is not directly dependent on the elements which are going to be inserted. It is all about the positions containing the elements. Therefore, the worst-case execution time of inserting n elements would be O(n).

##### 4. 

 Smallest complexity of my implementation would be again O(n) because the iterator **always** iterates trough all positions with a foreach-loop and there is no way to abort these iterations to get a smaller complexity.

### 3)

The execution time of this list not depends directly on the elements which are going to be inserted. It is all about the positions containing the elements. Therefore, the worst-case execution time of inserting n elements would be $\mathcal{O} (n)$ given this sum:

$$ \sum_{i=0}^{n} i $$

Sum for inserting $n$ elements. At the very first beginning it requires 0 iterations, as no element exists in the list yet. Afterwards 1 iterations, 2 iterations and so on.

### 4)

Smallest complexity of my implementation would be again $\mathcal{O} (n)$ because we **always** iterate trough all positions with a foreach-loop and there is no way to abort these iterations to get any smaller complexity.

<br/><br/><br/><br/><br/><br/><br/>

## Exercise 03

<img src="/home/andreas-imst/Developing/PS - Algorithmen und Datenstrukturen/Übungen/Woche 04 - Übung 04/Abgabe/src/ex03/ex03_a.png">

a. Which of the following examples are trees, which are not, why?

- 1 True, if d is assumed as the root node, otherwise not valid.
- 2 false, because an descendant cannot be a sibling of a ancestor
- 3 True, assuming that d is the root node and c is the direct child of d.
- 4 Is a valid tree, assuming that c is the root node and all following nodes are children in different levels

b. What are the differences between a generic tree and a binary tree? Why could a binary tree be useful?

A generic tree is also known as a General Tree. There is no limitation on how many children a node can actually have. A generic tree can't be empty and **must** have at least one child.
In contrast, a binary tree can be empty. Apart from that, it can only have **2 children** left and right at most. A binary tree always stores its nodes in an ordered way. Each right node **must** be greater than the root node and all left nodes **must** be smaller than the root node and so on.

A binary tree could be useful if you want to store values in an ordered way or sequence. Therefore it is way easier to store new values as you just have to compare each value of already stored nodes to the new value and cycle trough all nodes until there is no more child.

<img src="/home/andreas-imst/Developing/PS - Algorithmen und Datenstrukturen/Übungen/Woche 04 - Übung 04/Abgabe/src/ex03/binary_tree.png"> <img src="/home/andreas-imst/Developing/PS - Algorithmen und Datenstrukturen/Übungen/Woche 04 - Übung 04/Abgabe/src/ex03/general_tree.png">

c. Explain the terms siblings (Geschwister), child (Kind), internal node (interner Knoten), ancestor (Vorfahr) and descendant (Nachkomme), draw a tree and use it as an example to illustrate!

   1. Siblings always share the same parent node. Node **b** and **c** are children from node **a** and therefore they are siblings. Illustrated with **number 1**.
   2. Children are defined as nodes which have a parent node. Node **e** has one child. Illustrated with **number 2**.
   3. An internal node has at least one child node. **a** is the root node and has at least **two children**. Therefore it is an internal node. Illustrated with **number 3**.
   4. (Wikipedia) A node that is connected to all lower-level nodes is called an "ancestor". The connected lower-level nodes are "descendants" of the ancestor node. **c** is an ancestor from **i** and **i** is therefore a descendant of **c**. Illustrated with **number 4**.

<img src="/home/andreas-imst/Developing/PS - Algorithmen und Datenstrukturen/Übungen/Woche 04 - Übung 04/Abgabe/src/ex03/tree_illustration.png" style="width: 50%">

d. What does the height of a tree mean? How can you compute it?

A general definition of the height of a tree is the maximum depth of a node. A problem is when you examining the depth of all nodes, you come accross multiple nodes twice or more. This costs wasted operations.

Recursive method for determining the height of a tree in Java:

```Java
public int height(T tree) {
    int height = 0;
    for(Node v: tree.positions()) {
        if(tree.isExternal(v)) {
            h = max(h, depth(tree, v));
        }
    }
    return height;
}
```

A more effective way to determine the height of a tree is to start from the root node and examine the height of their relative children. It is not needed to cross multiple nodes twice or more anymore.

Recursive method for determining the height of a tree more effectively in Java:

```Java
public int height(T tree, Node v) {
    int height = 0;
    for(Node w: tree.children(v)) {
        h = max(h, 1 + height(tree, w));
    }
    return height;
}
```

e. What is the depth of a node and how can you compute it?

The depth of a node is defined on how many ancestor's a node have, expect the node itself. The computation works from the start of this node and for each ancestor you increment the depth by 1 and look for the depth of the parent node.

Recursive method for determining the depth of a node in Java:

```Java
public int depth(T tree, Node v) {
    if tree.isRoot(v) {
        return 0;
    }  else {
        return 1 + depth(tree, tree.parent(v));
    }
}
```

<br/><br/>

f. Compute the corresponding height giving each step of the computation for the tree

<img src="/home/andreas-imst/Developing/PS - Algorithmen und Datenstrukturen/Übungen/Woche 04 - Übung 04/Abgabe/src/ex03/ex03_tree.png">

| Step                                                                 | Result     |
| -------------------------------------------------------------------- | ---------- |
| 1. Root node is **a**.                                               | height = 0 |
| 2. The children of **a** are **b** and **c**.                        | 0 + 1 = 1  |
| 3. **c** is an external node, so **c** hasn't got any children left. |
| 4. The children of **b** are **d** and **e**.                        | 1 + 1 = 2  |
| 5. **d** is an external node.                                        |
| 6. The child of **e** is **f**                                       | 2 + 1  = 3 |

The height of this tree is **3**.

## Exercise 04

```Java
public class BracketMatching {

    private static char tmp;
    private static int position = 0;

    public static void main(String[] args) {
        System.out.println(bracketCaller("( {[ ]()})({[ ]() })"));
    }

    public static String bracketCaller(String brackets) {
        if (brackets.isEmpty()) {
            return "Given String is empty!";
        } else {
            // Removes whitespaces of given String
            brackets = brackets.replaceAll("\\s+", "");
            return bracketMatcher(brackets);
        }
    }
```

```Java
    public static String bracketMatcher(String brackets) {
        if (brackets.isEmpty()) {
            return "Is balanced!";
        } else if (brackets.length() == 1) {
            return String.valueOf(brackets.charAt(0));
        } else {
            tmp = brackets.charAt(position);
            return bracketMatcher(checkCharAfter(brackets));
        }
    }
```

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
```Java
    private static String checkCharAfter(String brackets) {
        StringBuilder sb = new StringBuilder(brackets);

        switch (brackets.charAt(position + 1)) {
            case '(': case '{': case '[':
                position++;
                break;
            default:
                switch (tmp) {
                    case '(':
                        if (brackets.charAt(position + 1) == ')') {
                            sb = sb.delete(position, position + 2);
                            if (position > 0) {
                                position--;
                            }
                            return sb.toString();
                        } else {
                            return String.valueOf(
                                brackets.charAt(position + 1));
                        }
                    case '[':
                        if (brackets.charAt(position + 1) == ']') {
                            sb = sb.delete(position, position + 2);
                            if (position > 0) {
                                position--;
                            }
                            return sb.toString();
                        } else {
                            return String.valueOf(
                                brackets.charAt(position + 1));
                        }
                    case '{':
                        if (brackets.charAt(position + 1) == '}') {
                            sb = sb.delete(position, position + 2);
                            if (position > 0) {
                                position--;
                            }
                            return sb.toString();
                        } else {
                            return String.valueOf(
                                brackets.charAt(position + 1));
                        }
                }
                break;
        }
        return brackets;
    }
}
```