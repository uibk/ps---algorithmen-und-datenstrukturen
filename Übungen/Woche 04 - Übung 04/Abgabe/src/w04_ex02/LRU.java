package w04_ex02;

public class LRU {

    public static void LRU(LinkedPositionalList<Integer> list, Integer item) {
        Position tmp = getExistingPosition(list, item);
        if (tmp == null) {
            list.addFirst(item);
        } else {
            list.remove(tmp);
            list.addFirst(item);
        }
    }

    public static void print(LinkedPositionalList<Integer> list) {
        System.out.println("\n----------");
        int i = 0;
        for (Integer item : list) {
            System.out.print(i);
            System.out.print(": ");
            System.out.println(item);
            i++;
        }
    }

    private static Position getExistingPosition(LinkedPositionalList<Integer> list, Integer item) {
        for (Position p : list.positions()) {
            if (p.getElement().equals(item)) {
                return p;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        LinkedPositionalList<Integer> list = new LinkedPositionalList<>();
        System.out.println("------");
        for (String item : args) {
            System.out.print(item);
            System.out.print(" ");
            LRU(list, Integer.parseInt(item));
        }
        print(list);
        System.out.println();
    }

}

