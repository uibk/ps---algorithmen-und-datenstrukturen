#### Exercise 02 ####

##### 2. 

In the following results I am explicitly talking about complexities from the $LRU$ function and not the whole $LRU$ class itself. 

```Java
public static void LRU(LinkedPositionalList<Integer> list, Integer item) {
        Position tmp = getExistingPosition(list, item);
        if (tmp == null) {
            list.addFirst(item);
        } else {
            list.remove(tmp);
            list.addFirst(item);
        }
    }
```

The worst-case of inserting elements occurs whether an element with the same value is already stored at the end of the list or a new element should be inserted.
We need to iterate trough all positions to determine the exact position of the element or if a position has already got an element with the same value. It ends up in a complexity of O(n).  

##### 3. 

The execution time of this list is not directly dependent on the elements which are going to be inserted. It is all about the positions containing the elements. Therefore, the worst-case execution time of inserting n elements would be O(n).

##### 4. 

 Smallest complexity of my implementation would be again O(n) because the iterator **always** iterates trough all positions with a foreach-loop and there is no way to abort these iterations to get a smaller complexity.
