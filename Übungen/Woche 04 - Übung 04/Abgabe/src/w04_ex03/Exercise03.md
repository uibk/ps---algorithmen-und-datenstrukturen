# Exercise 03

<img src="ex03_a.png">

a. Which of the following examples are trees, which are not, why?

- 1 True, if d is assumed as the root node, otherwise not valid.
- 2 false, because an descendant cannot be a sibling of a ancestor
- 3 True, assuming that d is the root node and c is the direct child of d.
- 4 Is a valid tree, assuming that c is the root node and all following nodes are children in different levels

b. What are the differences between a generic tree and a binary tree? Why could a binary tree be useful?

A generic tree is also known as a General Tree. There is no limitation on how many children a node can actually have. A generic tree can't be empty and **must** have at least one child.
In contrast a binary tree can be empty. Apart from that, it can only have **2 children** left and right at most. A binary tree always stores its nodes in an ordered way. Each right node **must** be greater than the root node and all left nodes **must** be smaller than the root node and so on.

A binary tree could be useful if you want to store values in an ordered way or sequence. Therefore it is way easier to store new values as you just have to compare each value of already stored nodes to the new value and cycle trough all nodes until there is no more child.

<img src="binary_tree.png"> <img src="general_tree.png">

c. Explain the terms siblings (Geschwister), child (Kind), internal node (interner Knoten), ancestor (Vorfahr) and descendant (Nachkomme), draw a tree and use it as an example to illustrate!

   1. Siblings always share the same parent node. Node **b** and **c** are children from node **a** and therefore they are siblings. Illustrated with number 1.
   2. Children are defined as nodes which have a parent node. Node **e** has one child. Illustrated with number 2.
   3. An internal node has at least one child node. **a** is the root node and has at least **two children**. Therefore it is an internal node. Illustrated with number 3.
   4. (Wikipedia) A node that is connected to all lower-level nodes is called an "ancestor". The connected lower-level nodes are "descendants" of the ancestor node. **c** is an ancestor from **i** and **i** is therefore a descendant of **c**. Illustrated with number 4.

<img src="tree_illustration.png" style="width: 50%">

d. What does the height of a tree mean? How can you compute it?

The height is defined in the maximum depth of a node. 

e. What is the depth of a node and how can you compute it?

The depth of a node is defined on how many ancestor's a node have, expect the node itself. The computation works from the start of the node and for each ancestor you increment the depth by 1 and look for the depth of the parent node.

Pseudo-Code in Java:

```Java
public int depth(T tree, Node v) {
    if tree.isRoot(v) {
        return 0;
    }  else {
        return 1 + depth(tree, tree.parent(v));
    }
}
```

f. Compute the corresponding height giving each step of the computation for the tree

<img src="ex03_tree.png">