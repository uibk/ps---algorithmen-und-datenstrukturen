package w04_ex04;

public class BracketMatching {

    private static char tmp;
    private static int position = 0;

    public static void main(String[] args) {
        System.out.println(bracketCaller("( {[ ]()})({[ ]() })"));
    }

    public static String bracketCaller(String brackets) {
        if (brackets.isEmpty()) {
            return "Given String is empty!";
        } else {
            brackets = brackets.replaceAll("\\s+", ""); // Removes whitespaces of given String
            return bracketMatcher(brackets);
        }
    }

    public static String bracketMatcher(String brackets) {
        if (brackets.isEmpty()) {
            return "Is balanced!";
        } else if (brackets.length() == 1) {
            return String.valueOf(brackets.charAt(0));
        } else {
            tmp = brackets.charAt(position);
            return bracketMatcher(checkCharAfter(brackets));
        }
    }

    private static String checkCharAfter(String brackets) {
        StringBuilder sb = new StringBuilder(brackets);

        switch (brackets.charAt(position + 1)) {
            case '(':
            case '{':
            case '[':
                position++;
                break;
            default:
                switch (tmp) {
                    case '(':
                        if (brackets.charAt(position + 1) == ')') {
                            sb = sb.delete(position, position + 2);
                            if (position > 0) {
                                position--;
                            }
                            return sb.toString();
                        } else {
                            return String.valueOf(brackets.charAt(position + 1));
                        }
                    case '[':
                        if (brackets.charAt(position + 1) == ']') {
                            sb = sb.delete(position, position + 2);
                            if (position > 0) {
                                position--;
                            }
                            return sb.toString();
                        } else {
                            return String.valueOf(brackets.charAt(position + 1));
                        }
                    case '{':
                        if (brackets.charAt(position + 1) == '}') {
                            sb = sb.delete(position, position + 2);
                            if (position > 0) {
                                position--;
                            }
                            return sb.toString();
                        } else {
                            return String.valueOf(brackets.charAt(position + 1));
                        }
                }
                break;
        }
        return brackets;
    }
}
