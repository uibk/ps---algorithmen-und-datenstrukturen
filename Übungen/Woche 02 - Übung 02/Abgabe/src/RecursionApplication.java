public class RecursionApplication {
    public static void main(String[] args) {
       decimalToBinary(25);
    }

    private static void decimalToBinary(int number) {
        if (number > 0) {
            decimalToBinary(number / 2);
            System.out.print(number % 2);
        }
    }
}
