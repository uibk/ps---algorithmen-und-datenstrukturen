# Andreas Murk - Week 03 Solutions #

## Exercise 1: Proof by Contradiction ##

Prove by contradiction that $2²^n\notin \mathcal{O}(2^n)$

The proposition illustrates that $A$ $\Rightarrow B$. Proof by contradiction says that $\neg{B}$ is inconsistent with $A$.

We assume that $B =$ $2²^n\notin \mathcal{O}(2^n)$.

So $\neg B$ would be $2²^n\in \mathcal{O}(2^n)$

We can take $A$ as conditions for the Big-O definition (it exists $c > 0$ and $n_{0} \ge 1$ $\forall$ $n \ge n_{0}$)

To prove this we have to assume that $\neg B$ $\Rightarrow$ $2²^n\in \mathcal{O}(2^n)$ is true.

$2²^n \le c$ $\mathcal{*}$ $2^n$ $\Rightarrow$ dividing both sides with $2^n$ we get $2^n \le c$ $\mathcal{*}$ $1$. When $c = 1$ the statement is false because $2^n > 1$ for this particular case. We have proven that:

$2²^n \le c$ $\mathcal{*}$ $2^n$ is false and $2²^n\notin \mathcal{O}(2^n)$ is wrong as we know that any asymptotic growth rate cannot be an element of any lowerbounds growth rates.

## Exercise 2: Proof by Induction ##

Prove by induction that the following is true:

$$f(n)=\sum_{i=1}^{n} i^{3} = \frac{n²(n+1)²}{4}$$

The base case of the induction is $n = 1$ which would be following:

$$ 1^{3} = \frac{1²(1+1)²}{4} =>  \frac{1(2)²}{4} = \frac{1\mathcal{*}4}{4} =1$$

We have proven when $n = 1$ it is true. The definition of induction says that when the base case $n = 1$ is true then for any other $n + 1$ the formula must be true as well.

$$\sum_{i=1}^{n} i^{3} + (n+1)³ = \frac{n²(n+1)²}{4} + (n+1)³ $$

The next step is to use the binomial theorem to transfer:
$$\frac{n²(n+1)²}{4} $$

into:

$$\frac{n²(n²+2n+1)}{4} = \frac{n⁴ + 2n³+n²}{4}$$

as well as the binomial theorem for:

$$(n+1)³ = n³+3n²+3n+1$$

The simplified term then looks like:

$$\frac{n⁴ + 2n³+n²}{4} + (n³+3n²+3n+1) $$

Next step is to get both terms to their common denominator:

$$\frac{n⁴ + 2n³+n²}{4} + \frac{4n³+12n²+12n+4}{4} $$

which then sums up to:

$$\frac{n⁴ + 2n³+n² + 4n³+12n²+12n+4}{4} = \frac{n⁴+6n³+13n²+12n+4}{4} $$

It concludes that these two terms are the same:

$$\frac{(n+1)²(n+2)²}{4} = \frac{n⁴+6n³+13n²+12n+4}{4} $$

Explanation here:

$$\frac{(n+1)²((n+1)+1)²}{4} = \frac{(n+1)²(n+2)²}{4} = \frac{(n²+2n+1)(n²+4n+4)}{4} = \frac{n⁴+4n³+4n²+2n³+8n²+8n+n²+4n+4}{4}$$

$$= \frac{n⁴+6n³+13n²+12n+4}{4}$$

So the statement is true:

$$ \frac{n⁴+6n³+13n²+12n+4}{4} =  \frac{n⁴+6n³+13n²+12n+4}{4} $$

## Exercise 3: Code Analysis ##

Determine the Big-O complexity of the following code fragment:

```Java
public static int example(int N) {
    int counter = 0;
    for(int i=2; i<=N; i=i*2) {
        for(int j=i; j<=N; j=j*2) {
            counter++;
        }
    }
    return counter;
}
```

We have two for loops which are iterating until the end `N` has reached. So `i<=N` and `j<=N` are both iterating until `N`. Normally, each loop would represent the complexity of $\mathcal{O}$($n$) and then multiplied $\mathcal{O}$($n$ $\mathcal{*}$ $n$) would result in $\mathcal{O}$($n$²).

Since the two index variables are incremented by $\mathcal{*}$ $2$ by the statements ``i = i * 2`` and ``j = j * 2`` the complexity of each loop is rather $\mathcal{O}$($log(n)$) because in each iteration the step is:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $p$ = one iteration step

$$ \frac{n}{2^p} = 1 => 2^p = n => p = log_{2}(n) $$

Tthe complexity of this algorithm would be then considered as:

$$\mathcal{O}(log_{2}(n) \mathcal{*}log_{2}(n)) = \mathcal{O}(log_{2}(n)²)$$

When we now take a look at the hierarchy of the asymptotic growth rates:

$$ \mathcal{O}(1) \subset \mathcal{O}(log(n)) \subset \mathcal{O}(n) \subset \mathcal{O}(n \mathcal{*} log(n)) \subset \mathcal{O}(n^a{^>¹}) \subset \mathcal{O}(2^n) \subset \mathcal{O}(n!)$$

we can see that $\mathcal{O}(log_{2}(n)²)$ hasn't got its own class of complexity. The Big-O definition ensures that for any growth rate all upperbounds must be valid. The next smallest upperbound is $\mathcal{O}(n)$. We test this with the following statement:

$$ log_{2}(n)² \le c \mathcal{*} n \in \mathcal{O}(n) $$

With the previous statement we have proven that the upperbound is valid.
Thus, the nearest complexity of the mentioned algorithm would be $\mathcal{O}(n)$.

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## Exercise 4: Recursion ##

```Java
public class RecursionApplication {
    public static void main(String[] args) {
        decimalToBinary(25);
    }

    private static void decimalToBinary(int number) {
        if (number > 0) {
            decimalToBinary(number / 2);
            System.out.print(number % 2);
        }
    }
}

Output in console:
11001
