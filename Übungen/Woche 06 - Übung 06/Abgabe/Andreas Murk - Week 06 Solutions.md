#### Exercise 02

<img src="src/ex02/img/1-4.png" style="width: 30%"><img src="src/ex02/img/5-6.png" style="width: 40%">
<img src="src/ex02/img/7-8.png" style="width: 40%">
<img src="src/ex02/img/9-11.png" style="width: 40%">
<img src="src/ex02/img/12.png" style="width: 70%">

#### Exercise 03

| Method    | Unsorted List    | Sorted List      | Heap                   |
| --------- | ---------------- | ---------------- | ---------------------- |
| size      | $\mathcal{O}(1)$ | $\mathcal{O}(1)$ | $\mathcal{O}(1)$       |
| isEmpty   | $\mathcal{O}(1)$ | $\mathcal{O}(1)$ | $\mathcal{O}(1)$       |
| insert    | $\mathcal{O}(1)$ | $\mathcal{O}(n)$ | $\mathcal{O}(log(n))$  |
| min       | $\mathcal{O}(n)$ | $\mathcal{O}(1)$ | $\mathcal{O}(1)$       |
| removeMin | $\mathcal{O}(n)$ | $\mathcal{O}(1)$ | $\mathcal{O}(log (n))$ |

#### Exercise 04

##### Insertion Sort

<img src="src/w04_ex04/img/insertion_sort.png">

<br/>
<br/>
<br/>

##### Selection Sort

<img src="src/w04_ex04/img/selection_sort.png">

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

##### Initial Heap bottom-up construction

<img src="src/w04_ex04/img/heap_sort_initial.png">

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

### Heap Sort

<img src="src/w04_ex04/img/heap_sort_1.png" style="width:90%">

<img src="src/w04_ex04/img/heap_sort_2.png" style="width: 50%">

<img src="src/w04_ex04/img/heap_sort_3.png" style="width: 45%">
