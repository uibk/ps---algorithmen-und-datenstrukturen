package w06_ex01;

import java.util.PriorityQueue;

public class EmergencyRoom {

    public PriorityQueue<Patient> priorityQueue;

    public EmergencyRoom() {
        this.priorityQueue = new PriorityQueue<>();
    }

    public void registerPatient(Patient patient) {
        System.out.println(patient.getName() + " is admitted.");
        priorityQueue.add(patient);
    }

    public Patient treatPatient() {
        return priorityQueue.poll();
    }

    public int size() {
        return priorityQueue.size();
    }
}
