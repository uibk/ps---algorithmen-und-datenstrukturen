package w06_ex01;

public class Main {
    public static void main(String[] args) {
        EmergencyRoom emergencyRoom = new EmergencyRoom();
        Patient[] patientList = {
                new Patient(1L, "John Alpha", Severity.SERIOUS, 0L, true),
                new Patient(2L, "Mark Bravo", Severity.SERIOUS, 1L, false),
                new Patient(3L, "Steven Charlie", Severity.SEVERE, 1L, false),
                new Patient(4L, "Christian Delta", Severity.MODERATE, 2L, true),
                new Patient(5L, "Matthew Echo", Severity.MODERATE, 2L, false),
                new Patient(6L, "Sebastian Foxtrott", Severity.SEVERE, 4L, false),
                new Patient(7L, "Anna Golf", Severity.SEVERE, 4L, true),
                new Patient(8L, "Maria Hotel", Severity.SERIOUS, 5L, false),
        };

        // Add first 4 patients to emergency room
        for (int i = 0; i < 4; i++) {
            emergencyRoom.registerPatient(patientList[i]);
        }
        System.out.println("\nNext Treatment:");
        System.out.println(emergencyRoom.treatPatient());

        emergencyRoom.registerPatient(patientList[4]);

        System.out.println("\nNext Treatment:");
        System.out.println(emergencyRoom.treatPatient());


        System.out.println("\nNext Treatment:");
        System.out.println(emergencyRoom.treatPatient());

        // Add remaining patients
        for (int i = 5; i < patientList.length; i++) {
            emergencyRoom.registerPatient(patientList[i]);
        }

        System.out.println("\nThe emergency room closes. All patients that were admitted so far are treated sequentially");
        int leftPatients = emergencyRoom.size();
        for (int i = 0; i < leftPatients; i++) {
            System.out.println(emergencyRoom.treatPatient());
        }
    }
}

