package w06_ex01;

public enum Severity {

    SEVERE,
    SERIOUS,
    MODERATE;

    Severity() {
    }
}
