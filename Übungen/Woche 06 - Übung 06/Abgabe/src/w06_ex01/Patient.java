package w06_ex01;

public class Patient implements Comparable<Patient> {

    private Long id;
    private String name;
    private Severity severity;
    private Long admissionTime;
    private boolean insurance;

    public Patient(Long id, String name, Severity severity, Long admissionTime, boolean insurance) {
        this.id = id;
        this.name = name;
        this.severity = severity;
        this.admissionTime = admissionTime;
        this.insurance = insurance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public Long getAdmissionTime() {
        return admissionTime;
    }

    public void setAdmissionTime(Long admissionTime) {
        this.admissionTime = admissionTime;
    }

    public boolean isInsurance() {
        return insurance;
    }

    public void setInsurance(boolean insurance) {
        this.insurance = insurance;
    }

    @Override
    public int compareTo(Patient patient) {
        if (this.severity.compareTo(patient.severity) == 0 && this.admissionTime == patient.admissionTime && this.insurance == patient.insurance) {
            return 0;
        } else if (this.severity.compareTo(patient.severity) < 0
                || this.severity.compareTo(patient.severity) == 0 && this.admissionTime < patient.admissionTime
                || this.severity.compareTo(patient.severity) == 0 && this.admissionTime == patient.admissionTime && this.insurance) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "Patient: " + this.getName() + "\n" +
                "ID: " + this.getId() + "\nSeverity: " + this.getSeverity() + "\nTime of Admission: " + this.getAdmissionTime() + "\nInsurance: " + this.isInsurance() + "\n\n";
    }
}
